package com.rec.task.services

import com.rec.task.components.interfaces.ExchangeComponent
import com.rec.task.domain.AccType
import com.rec.task.domain.Account
import com.rec.task.domain.AppUser
import com.rec.task.domain.SubAccount
import com.rec.task.domain.dto.ExchangeInfoDto
import com.rec.task.domain.errors.ExchangeServiceException
import com.rec.task.repository.AppUserRepository
import spock.lang.Specification

class AccountServiceImplSpec extends Specification {

    def repository = Mock(AppUserRepository)

    def exchangeComponent = Mock(ExchangeComponent)

    def service = new AccountServiceImpl(repository, exchangeComponent)

    def "exchangeCurrency: should throw exception if user not found"() {
        given:
           repository.findByPersonalNumber(*_) >> Optional.empty()
        when:
           service.exchangeCurrency(new ExchangeInfoDto('123', null, null, null))
        then:
           thrown(ExchangeServiceException)
    }

    def "exchangeCurrency: should throw exception if selected the same currency"() {
        given:
           repository.findByPersonalNumber(*_) >> Optional.of(new AppUser())
        when:
           service.exchangeCurrency(new ExchangeInfoDto('123', null, soure, target))
        then:
           thrown(ExchangeServiceException)
        where:
           soure << ['usd', 'pln']
           target << ['usd', 'pln']
    }

    def "exchangeCurrency: should throw exception if not enough money on source sub account"() {
        given:
           def subPln = new SubAccount(null, BigDecimal.TEN, AccType.PLN, null)
        and:
           def subUsd = new SubAccount(null, BigDecimal.TEN, AccType.USD, null)
        and:
           def user = new AppUser(null, null, null, '123', new Account(null, null, [subPln, subUsd] as Set))
        and:
           repository.findByPersonalNumber('123') >> Optional.of(user)
        when:
           service.exchangeCurrency(new ExchangeInfoDto('123', BigDecimal.valueOf(11), source, 'target'))
        then:
           thrown(ExchangeServiceException)
        where:
           source << ['usd', 'UsD', 'USD', 'plN', 'PLN', 'pln']
    }

    def "exchangeCurrency: should throw exception failed to calculate new value"() {
        given:
           def subPln = new SubAccount(null, BigDecimal.TEN, AccType.PLN, null)
        and:
           def subUsd = new SubAccount(null, BigDecimal.TEN, AccType.USD, null)
        and:
           def user = new AppUser(null, null, null, '123', new Account(null, null, [subPln, subUsd] as Set))
        and:
           repository.findByPersonalNumber('123') >> Optional.of(user)
        and:
           exchangeComponent.exchange(*_) >> { throw new RuntimeException('error') }
        when:
           service.exchangeCurrency(new ExchangeInfoDto('123', BigDecimal.ONE, 'usd', 'pln'))
        then:
           thrown(ExchangeServiceException)
    }

    def "exchangeCurrency: should save new values"() {
        given:
           def subPln = new SubAccount(null, BigDecimal.TEN, AccType.PLN, null)
        and:
           def subUsd = new SubAccount(null, BigDecimal.TEN, AccType.USD, null)
        and:
           def user = new AppUser(null, null, null, '123', new Account(null, null, [subPln, subUsd] as Set))
        and:
           repository.findByPersonalNumber('123') >> Optional.of(user)
        and:
           exchangeComponent.exchange(*_) >> Optional.of(BigDecimal.ONE)
        when:
           service.exchangeCurrency(new ExchangeInfoDto('123', BigDecimal.ONE, source, target))
        then:
           1 * repository.save({
               with(it as AppUser) {
                   it.getAccount().getSubAccounts().findAll({
                       if (it.type == AccType.PLN) {
                           it.amount == newPln
                       } else if (it.type == AccType.USD) {
                           it.amount == newUsd
                       } else {
                           1 == 0
                       }
                   }).isEmpty()
               }
           })
        where:
           source << ['usd', 'pln']
           target << ['pln', 'usD']
           newPln << [BigDecimal.valueOf(11), BigDecimal.valueOf(9)]
           newUsd << [BigDecimal.valueOf(9), BigDecimal.valueOf(11)]
    }
}
