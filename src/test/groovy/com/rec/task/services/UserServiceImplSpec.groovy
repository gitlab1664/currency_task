package com.rec.task.services

import com.rec.task.domain.AccType
import com.rec.task.domain.AppUser
import com.rec.task.domain.dto.RegisterInfoDto
import com.rec.task.domain.dto.RegisterUserDto
import com.rec.task.domain.errors.UserExistsException
import com.rec.task.repository.AppUserRepository
import spock.lang.Specification

class UserServiceImplSpec extends Specification {

    def repository = Mock(AppUserRepository)

    def service = new UserServiceImpl(repository)

    def "registerUser: should add new user"() {
        given:
           def userDto = new RegisterUserDto('28092773936', 'firstName', 'lastName')
        and:
           def registerDto = new RegisterInfoDto(userDto, 1000.0)
        and:
           repository.findByPersonalNumber('28092773936') >> Optional.empty()
        when:
           def result = service.registerUser(registerDto)
        then:
           result.isPresent()
        and:
           1 * repository.save({
               with(it as AppUser) {
                   it.getFirstName() == 'firstName'
                   it.getLastName() == 'lastName'
                   it.getPersonalNumber() == '28092773936'
                   it.getAccount() != null
                   it.getAccount().getSubAccounts() != null
                   it.getAccount().getSubAccounts().size() == 2
                   it.getAccount().getSubAccounts().findAll({
                       if (it.type == AccType.PLN) {
                           it.amount == 1000.0
                       } else if (it.type == AccType.USD) {
                           it.amount == 0
                       } else {
                           1 == 0
                       }
                   }).isEmpty()
               }
           })
        and:
           result.isPresent()
        and:
           result.get().firstName == 'firstName'
           result.get().lastName == 'lastName'
           result.get().getAccount() != null
           result.get().getAccount().getSubAccounts().size() == 2
    }

    def "registerUser: should throw user exists"() {
        given:
           repository.findByPersonalNumber(*_) >> Optional.of(new AppUser())
        and:
           def userDto = new RegisterUserDto('28092773936', 'firstName', 'lastName')
        and:
           def registerDto = new RegisterInfoDto(userDto, 1000.0)
        when:
           service.registerUser(registerDto)
        then:
           thrown(UserExistsException)
    }
}
