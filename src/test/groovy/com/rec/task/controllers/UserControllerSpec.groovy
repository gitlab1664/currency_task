package com.rec.task.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import com.rec.task.domain.AccType
import com.rec.task.domain.dto.*
import com.rec.task.services.interfaces.UserService
import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.hamcrest.Matchers.hasSize
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(UserController)
class UserControllerSpec extends Specification {

    @Autowired
    MockMvc mockMvc

    ObjectMapper mapper = new ObjectMapper()

    @SpringBean
    UserService service = Mock(UserService)

    def "registerNewUser: should validate user first name and last name"() {
        given:
           def userDto = new RegisterUserDto('52022974755', firstName, lastName)
        and:
           def registerDto = new RegisterInfoDto(userDto, BigDecimal.ZERO)
        when:
           def result = mockMvc.perform(post('/api/v1/user')
                   .contentType(MediaType.APPLICATION_JSON)
                   .content(mapper.writeValueAsString(registerDto)))
        then:
           result.andExpect(status().isBadRequest())
        and:
           result.andExpect(jsonPath('$.result').value(false))
        and:
           result.andExpect(jsonPath('$.errors').isArray())
        and:
           result.andExpect(jsonPath('$.errors', hasSize(errorsSize)))
        where:
           firstName << ['', '  ', null]
           lastName << ['', '  ', null]
           errorsSize << [2, 2, 4]
    }

    def "registerNewUser: should validate user age"() {
        given:
           def userDto = new RegisterUserDto(personalNumber, 'firstName', 'lastName')
        and:
           def registerDto = new RegisterInfoDto(userDto, BigDecimal.ZERO)
        when:
           def result = mockMvc.perform(post('/api/v1/user')
                   .contentType(MediaType.APPLICATION_JSON)
                   .content(mapper.writeValueAsString(registerDto)))
        then:
           result.andExpect(status().isBadRequest())
        and:
           result.andExpect(jsonPath('$.result').value(false))
        and:
           result.andExpect(jsonPath('$.errors').isArray())
        and:
           result.andExpect(jsonPath('$.errors[0]').value('user.personalNumber invalid age, user must be of legal age (+18)'))
        where:
           personalNumber << ['15302560714', '05302576110']
    }

    def "registerNewUser: should validate if personal number"() {
        given:
           def userDto = new RegisterUserDto(personalNumber, 'firstName', 'lastName')
        and:
           def registerDto = new RegisterInfoDto(userDto, BigDecimal.ZERO)
        when:
           def result = mockMvc.perform(post('/api/v1/user')
                   .contentType(MediaType.APPLICATION_JSON)
                   .content(mapper.writeValueAsString(registerDto)))
        then:
           result.andExpect(status().isBadRequest())
        and:
           result.andExpect(jsonPath('$.result').value(false))
        and:
           result.andExpect(jsonPath('$.errors').isArray())
        and:
           result.andExpect(jsonPath('$.errors', hasSize(2)))
        where:
           personalNumber << ['', ' ', null, '111111111']
    }

    def "registerNewUser: should validate initial amount"() {
        given:
           def userDto = new RegisterUserDto('28092773936', 'firstName', 'lastName')
        and:
           def registerDto = new RegisterInfoDto(userDto, amount)
        when:
           def result = mockMvc.perform(post('/api/v1/user')
                   .contentType(MediaType.APPLICATION_JSON)
                   .content(mapper.writeValueAsString(registerDto)))
        then:
           result.andExpect(status().isBadRequest())
        and:
           result.andExpect(jsonPath('$.result').value(false))
        and:
           result.andExpect(jsonPath('$.errors').isArray())
        and:
           result.andExpect(jsonPath('$.errors', hasSize(1)))
        where:
           amount << [-123.0, null]
    }

    def "registerNewUser: should create user"() {
        given:
           def userDto = new RegisterUserDto('28092773936', 'firstName', 'lastName')
        and:
           def registerDto = new RegisterInfoDto(userDto, 1000.0)
        and:
           def acc = new AccountDto([new SubAccountDto(1000.0, AccType.PLN), new SubAccountDto(0.00, AccType.USD)])
        and:
           def appUserDto = new AppUserDto('firstName', 'lastName', '28092773936', acc)
        and:
           service.registerUser(*_) >> Optional.of(appUserDto)
        when:
           def result = mockMvc.perform(post('/api/v1/user')
                   .contentType(MediaType.APPLICATION_JSON)
                   .content(mapper.writeValueAsString(registerDto)))
        then:
           result.andExpect(status().isCreated())
        and:
           result.andExpect(jsonPath('$.firstName').value('firstName'))
        and:
           result.andExpect(jsonPath('$.lastName').value('lastName'))
        and:
           result.andExpect(jsonPath('$.personalNumber').value('28092773936'))
        and:
           result.andExpect(jsonPath('$.account.subAccounts', hasSize(2)))
    }

    def "getUserInfo: should return user"() {
        given:
           def acc = new AccountDto([new SubAccountDto(1000.0, AccType.PLN), new SubAccountDto(0.00, AccType.USD)])
        and:
           def appUserDto = new AppUserDto('firstName', 'lastName', '28092773936', acc)
        and:
           service.getUser(*_) >> Optional.of(appUserDto)
        when:
           def result = mockMvc.perform(post('/api/v1/user/{personalNumber}', '12345')
                   .contentType(MediaType.APPLICATION_JSON))
        then:
           result.andExpect(status().isOk())
        and:
           result.andExpect(jsonPath('$.firstName').value('firstName'))
        and:
           result.andExpect(jsonPath('$.lastName').value('lastName'))
        and:
           result.andExpect(jsonPath('$.personalNumber').value('28092773936'))
        and:
           result.andExpect(jsonPath('$.account.subAccounts', hasSize(2)))
    }

    def "getUserInfo: should return not found"() {
        given:
           service.getUser(*_) >> Optional.empty()
        when:
           def result = mockMvc.perform(post('/api/v1/user/{personalNumber}', '12345')
                   .contentType(MediaType.APPLICATION_JSON))
        then:
           result.andExpect(status().isNotFound())
    }

}
