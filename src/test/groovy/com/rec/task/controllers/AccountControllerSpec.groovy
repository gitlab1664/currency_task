package com.rec.task.controllers

import com.rec.task.domain.AccType
import com.rec.task.domain.dto.AccountDto
import com.rec.task.domain.dto.SubAccountDto
import com.rec.task.services.interfaces.AccountService
import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.hamcrest.Matchers.hasSize
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(AccountController)
class AccountControllerSpec extends Specification {

    @Autowired
    MockMvc mockMvc

    @SpringBean
    AccountService accountService = Mock(AccountService)

    def "getAccountInfo: should return ok"() {
        given:
           def acc = new AccountDto([new SubAccountDto(1000.0, AccType.PLN), new SubAccountDto(0.00, AccType.USD)])
        and:
           accountService.getAccountInfo(*_) >> Optional.of(acc)
        when:
           def result = mockMvc.perform(post('/api/v1/account/{personalNumber}', '1234')
                   .contentType(MediaType.APPLICATION_JSON))
        then:
           result.andExpect(status().isOk())
        and:
           result.andExpect(jsonPath('$.subAccounts', hasSize(2)))
    }

    def "getAccountInfo: should return not found"() {
        given:
           accountService.getAccountInfo(*_) >> Optional.empty()
        when:
           def result = mockMvc.perform(post('/api/v1/account/{personalNumber}', '1234')
                   .contentType(MediaType.APPLICATION_JSON))
        then:
           result.andExpect(status().isNotFound())
    }
}
