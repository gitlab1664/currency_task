package com.rec.task.clients

import com.rec.task.domain.CurrencyRate
import com.rec.task.domain.CurrencyResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

class CurrencyClientImplSpec extends Specification {

    def template = Mock(RestTemplate)

    def client = new CurrencyClientImpl(template)

    def "getCurrentValue: should return empty on exception"() {
        given:
           template.getForEntity(*_) >> { throw new RuntimeException("error") }
        when:
           def result = client.getCurrentValue('USD')
        then:
           result.isEmpty()
    }

    def "getCurrentValue: should return empty on no body"() {
        given:
           def response = new CurrencyResponse()
        and:
           template.getForEntity(*_) >> new ResponseEntity<>(response, status)
        when:
           def result = client.getCurrentValue('usd')
        then:
           result.isEmpty()
        where:
           status << [HttpStatus.NOT_FOUND, HttpStatus.CONFLICT]
    }

    def "getCurrentValue: should return currency value"() {
        given:
           def rate = new CurrencyRate(BigDecimal.TEN)
        and:
           def response = new CurrencyResponse('usd', [rate])
        and:
           template.getForEntity(*_) >> new ResponseEntity<Object>(response, HttpStatus.OK)
        when:
           def result = client.getCurrentValue('usd')
        then:
           result.isPresent()
        and:
           result.get() == BigDecimal.TEN
    }
}
