package com.rec.task.components

import com.rec.task.clients.CurrencyClient
import com.rec.task.domain.errors.ExchangeServiceException
import spock.lang.Specification

class ExchangeComponentImplSpec extends Specification {

    def client = Mock(CurrencyClient)

    def component = new ExchangeComponentImpl(client)

    def "exchange: should throw exception if getting actual currency value failed"() {
        given:
           client.getCurrentValue(*_) >> Optional.empty()
        when:
           component.exchange('usd', BigDecimal.TEN)
        then:
           thrown(ExchangeServiceException)
    }

    def "exchange: should return calculated value"() {
        given:
           client.getCurrentValue(*_) >> Optional.of(BigDecimal.TEN)
        when:
           def result = component.exchange(source, BigDecimal.valueOf(5L))
        then:
           result.isPresent()
        and:
           result.get() == shouldBe
        where:
           source << ['PLN', 'USD']
           shouldBe << [BigDecimal.valueOf(0.5), BigDecimal.valueOf(50)]
    }
}
