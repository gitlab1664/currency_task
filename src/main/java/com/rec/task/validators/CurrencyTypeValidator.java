package com.rec.task.validators;

import com.rec.task.domain.AccType;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Slf4j
public class CurrencyTypeValidator implements ConstraintValidator<CurrencyTypeConstraint, String> {

    @Override
    public void initialize(CurrencyTypeConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(String input, ConstraintValidatorContext constraintValidatorContext) {
        try {
            AccType.valueOf(input.toUpperCase());
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
