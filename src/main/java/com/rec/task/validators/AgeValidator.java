package com.rec.task.validators;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.Period;

@Slf4j
public class AgeValidator implements ConstraintValidator<AgeConstraint, String> {
    @Override
    public void initialize(AgeConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(String input, ConstraintValidatorContext constraintValidatorContext) {
        if (StringUtils.isEmpty(input)){
            //ignore other validators will force proper value
            return true;
        }
        if (StringUtils.isEmpty(input.trim())){
            //ignore other validators will force proper value
            return true;
        }

        try {
            LocalDate birthDate = calculateBirthDate(input);
            Period period = Period.between(birthDate, LocalDate.now());
            log.debug("age: {}", period.getYears());
            if (period.getYears() >= 18) {
                return true;
            }
        } catch (Exception e) {
            //ignore other validators will force proper value
        }
        return false;
    }

    private LocalDate calculateBirthDate(String input) throws DateTimeException {
        int[] numberAsIntArray = new int[11];
        for (int i = 0; i < 11; i++) {
            numberAsIntArray[i] = Character.getNumericValue(input.charAt(i));
        }

        int birthDay = 10 * numberAsIntArray[4] + numberAsIntArray[5];
        int birthYear = 10 * numberAsIntArray[0] + numberAsIntArray[1];
        int birthMonth = 10 * numberAsIntArray[2] + numberAsIntArray[3];

        if (birthMonth <= 12) birthYear += 1900;
        else if (birthMonth <= 32) {
            birthYear += 2000;
            birthMonth -= 20;
        } else if (birthMonth <= 52) {
            birthYear += 2100;
            birthMonth -= 40;
        } else if (birthMonth <= 72) {
            birthYear += 2200;
            birthMonth -= 60;
        } else if (birthMonth <= 92) {
            birthYear += 1800;
            birthMonth -= 80;
        }

        return LocalDate.of(birthYear, birthMonth, birthDay);
    }

}
