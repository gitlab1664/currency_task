package com.rec.task.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = CurrencyTypeValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface CurrencyTypeConstraint {
    String message() default "invalid currency type, possible values USD or PLN";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
