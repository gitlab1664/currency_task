package com.rec.task.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = AgeValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface AgeConstraint {
    String message() default "invalid age, user must be of legal age (+18)";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
