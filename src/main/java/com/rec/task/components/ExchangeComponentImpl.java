package com.rec.task.components;

import com.rec.task.clients.CurrencyClient;
import com.rec.task.components.interfaces.ExchangeComponent;
import com.rec.task.domain.errors.ExchangeServiceException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ExchangeComponentImpl implements ExchangeComponent {

    private final CurrencyClient currencyClient;

    @Override
    public Optional<BigDecimal> exchange(String source, BigDecimal amount) {

        Optional<BigDecimal> currentUsd = currencyClient.getCurrentValue("USD");

        if (currentUsd.isEmpty()) {
            throw new ExchangeServiceException("unable to get actual currencies values, please try again later");
        }

        if (source.equalsIgnoreCase("USD")) {
            return Optional.of(amount.multiply(currentUsd.get()));
        } else {
            return Optional.of(amount.divide(currentUsd.get(),5, RoundingMode.CEILING));

        }
    }
}
