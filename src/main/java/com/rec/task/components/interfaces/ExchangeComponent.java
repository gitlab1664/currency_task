package com.rec.task.components.interfaces;

import java.math.BigDecimal;
import java.util.Optional;

public interface ExchangeComponent {
    Optional<BigDecimal> exchange(String source, BigDecimal amount);


}
