package com.rec.task.controllers;

import com.rec.task.domain.dto.ExchangeInfoDto;
import com.rec.task.services.interfaces.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("api/v1/account")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @GetMapping
    @RequestMapping("{personalNumber}")
    public ResponseEntity<?> getAccountInfo(@PathVariable String personalNumber) {
        return accountService.getAccountInfo(personalNumber).map(ResponseEntity::ok)
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PatchMapping
    public ResponseEntity<?> exchange(@Valid @RequestBody ExchangeInfoDto exchangeInfoDto) {
        return accountService.exchangeCurrency(exchangeInfoDto).map(ResponseEntity::ok)
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
