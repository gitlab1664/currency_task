package com.rec.task.controllers.handlers;

import com.rec.task.domain.errors.ErrorMessage;
import com.rec.task.domain.errors.ExchangeServiceException;
import com.rec.task.domain.errors.UserExistsException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
@ControllerAdvice
public class ErrorHandler {

    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorMessage handleRequestValidationErrors(MethodArgumentNotValidException e) {
        List<String> errors = new ArrayList<>();

        e.getBindingResult()
                .getAllErrors()
                .forEach(error -> errors.add(((FieldError) error).getField() + " " + error.getDefaultMessage()));

        return ErrorMessage.builder().result(false)
                .timeStamp(LocalDateTime.now())
                .errors(errors).build();
    }

    @ResponseBody
    @ExceptionHandler(UserExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ErrorMessage handleUserExists(UserExistsException e) {
        return ErrorMessage.builder()
                .result(false)
                .timeStamp(LocalDateTime.now())
                .errors(Collections.singletonList(e.getMessage()))
                .build();
    }

    @ResponseBody
    @ExceptionHandler(ExchangeServiceException.class)
    @ResponseStatus(HttpStatus.PRECONDITION_FAILED)
    public ErrorMessage handleExchangeServiceErrors(ExchangeServiceException e) {
        return ErrorMessage.builder()
                .result(false)
                .timeStamp(LocalDateTime.now())
                .errors(Collections.singletonList(e.getMessage()))
                .build();
    }
}
