package com.rec.task.controllers;

import com.rec.task.domain.dto.AppUserDto;
import com.rec.task.domain.dto.RegisterInfoDto;
import com.rec.task.services.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("api/v1/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping
    public ResponseEntity<?> registerNewUser(@Valid @RequestBody RegisterInfoDto registerInfoDto) {
        Optional<AppUserDto> result = userService.registerUser(registerInfoDto)
                .map(appUserDto -> appUserDto.add(linkTo(methodOn(UserController.class).getUserInfo(appUserDto.getPersonalNumber())).withSelfRel()));
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @GetMapping
    @RequestMapping("{personalNumber}")
    public ResponseEntity<?> getUserInfo(@PathVariable String personalNumber) {
        Optional<Object> result = userService.getUser(personalNumber)
                .map(appUserDto -> appUserDto.add(linkTo(methodOn(UserController.class).getUserInfo(appUserDto.getPersonalNumber())).withSelfRel()));
        if (result.isPresent()) {
            return new ResponseEntity<>(result, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
