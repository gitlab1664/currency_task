package com.rec.task.services.interfaces;

import com.rec.task.domain.dto.AccountDto;
import com.rec.task.domain.dto.ExchangeInfoDto;

import java.util.Optional;

public interface AccountService {

    Optional<AccountDto> getAccountInfo(String personalNumber);

    Optional<AccountDto> exchangeCurrency(ExchangeInfoDto exchangeInfoDto);
}
