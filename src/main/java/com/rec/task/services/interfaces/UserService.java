package com.rec.task.services.interfaces;

import com.rec.task.domain.dto.AppUserDto;
import com.rec.task.domain.dto.RegisterInfoDto;

import java.util.Optional;

public interface UserService {

    Optional<AppUserDto> registerUser(RegisterInfoDto registerInfo);

    Optional<AppUserDto> getUser(String personalNumber);
}
