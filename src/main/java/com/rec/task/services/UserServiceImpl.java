package com.rec.task.services;

import com.rec.task.domain.*;
import com.rec.task.domain.dto.AppUserDto;
import com.rec.task.domain.dto.RegisterInfoDto;
import com.rec.task.domain.errors.UserExistsException;
import com.rec.task.mappers.AppUserMapper;
import com.rec.task.mappers.RegisterInfoMapper;
import com.rec.task.repository.AppUserRepository;
import com.rec.task.services.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final RegisterInfoMapper mapper = RegisterInfoMapper.INSTANCE;

    private final AppUserMapper appUserMapper = AppUserMapper.INSTANCE;

    private final AppUserRepository repository;

    @Override
    public Optional<AppUserDto> registerUser(RegisterInfoDto registerInfo) {

        repository.findByPersonalNumber(registerInfo.getUser().getPersonalNumber()).ifPresent(appUser -> {
            throw new UserExistsException("User already exists");
        });

        RegisterInfo register = mapper.fromDto(registerInfo);

        AppUser userToSave = register.getUser();
        Account acc = new Account();
        acc.setUser(userToSave);
        acc.addSubAccount(SubAccount.builder().amount(register.getAmount().setScale(4, RoundingMode.CEILING)).type(AccType.PLN).build());
        acc.addSubAccount(SubAccount.builder().amount(BigDecimal.ZERO).type(AccType.USD).build());
        userToSave.setAccount(acc);

        repository.save(userToSave);

        return Optional.of(appUserMapper.toDto(userToSave));
    }

    @Override
    public Optional<AppUserDto> getUser(String personalNumber) {
        return repository.findByPersonalNumber(personalNumber).map(appUserMapper::toDto);
    }
}
