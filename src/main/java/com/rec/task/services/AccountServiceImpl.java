package com.rec.task.services;

import com.rec.task.components.interfaces.ExchangeComponent;
import com.rec.task.domain.AccType;
import com.rec.task.domain.AppUser;
import com.rec.task.domain.dto.AccountDto;
import com.rec.task.domain.dto.ExchangeInfoDto;
import com.rec.task.domain.errors.ExchangeServiceException;
import com.rec.task.mappers.AccountMapper;
import com.rec.task.repository.AppUserRepository;
import com.rec.task.services.interfaces.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.RoundingMode;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AppUserRepository repository;

    private final ExchangeComponent exchangeComponent;

    private final AccountMapper mapper = AccountMapper.INSTANCE;

    @Override
    public Optional<AccountDto> getAccountInfo(String personalNumber) {
        return repository.findByPersonalNumber(personalNumber)
                .map(appUser -> mapper.toDto(appUser.getAccount()));
    }

    @Override
    public Optional<AccountDto> exchangeCurrency(ExchangeInfoDto exchangeInfoDto) {
        Optional<AppUser> user = repository.findByPersonalNumber(exchangeInfoDto.getUserNumber());
        if (user.isEmpty()) {
            throw new ExchangeServiceException("user not found");
        }

        if (exchangeInfoDto.getSourceCurrency().equalsIgnoreCase(exchangeInfoDto.getTargetCurrency())) {
            throw new ExchangeServiceException("two different currencies must be selected");
        }

        user.get().getAccount().getSubAccounts().stream()
                .filter(subAccount -> subAccount.getType() == AccType.valueOf(exchangeInfoDto.getSourceCurrency().toUpperCase())).findFirst()
                .ifPresent(subAccount -> {
                    if (subAccount.getAmount().compareTo(exchangeInfoDto.getAmount()) < 0) {
                        throw new ExchangeServiceException("not enough resources on source sub account");
                    }

                });

        try {
            exchangeComponent.exchange(exchangeInfoDto.getSourceCurrency(), exchangeInfoDto.getAmount()).ifPresent(result -> {
                user.get().getAccount().getSubAccounts().forEach(subAccount -> {
                    if (subAccount.getType() == AccType.valueOf(exchangeInfoDto.getTargetCurrency().toUpperCase())) {
                        subAccount.setAmount(subAccount.getAmount().add(result).setScale(4, RoundingMode.CEILING));
                    } else {
                        subAccount.setAmount(subAccount.getAmount().subtract(exchangeInfoDto.getAmount()).setScale(4, RoundingMode.CEILING));
                    }
                });
                repository.save(user.get());
            });
        } catch (Exception exception) {
            log.error("", exception);
            throw new ExchangeServiceException("unexpected error accrued, please try again later");
        }

        return Optional.of(mapper.toDto(user.get().getAccount()));
    }
}
