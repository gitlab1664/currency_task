package com.rec.task.clients;

import java.math.BigDecimal;
import java.util.Optional;

public interface CurrencyClient {

    Optional<BigDecimal> getCurrentValue(String currency);
}
