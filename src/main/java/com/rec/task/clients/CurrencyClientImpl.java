package com.rec.task.clients;

import com.rec.task.domain.CurrencyResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Optional;

@Slf4j
@Component
@RequiredArgsConstructor
public class CurrencyClientImpl implements CurrencyClient {

    private final RestTemplate restTemplate;

    @Value("${task.currency-api:http://api.nbp.pl}")
    private String currencyApi;

    @Override
    public Optional<BigDecimal> getCurrentValue(String currency) {
        ResponseEntity<CurrencyResponse> result;
        try {
            result = restTemplate.getForEntity(currencyApi + "/api/exchangerates/rates/A/" + currency, CurrencyResponse.class);
        } catch (Exception ex) {
            log.error("", ex);
            return Optional.empty();
        }

        if (result.getBody() != null) {
            if (result.getBody().getRates() != null && result.getBody().getRates().size() >= 1) {
                return Optional.of(result.getBody().getRates().get(0).getMid());
            }
        }

        return Optional.empty();
    }
}
