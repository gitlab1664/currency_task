package com.rec.task.domain;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SubAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private Long id;

    @Column(precision=16, scale=4)
    private BigDecimal amount;

    @Enumerated(EnumType.STRING)
    private AccType type;

    @ManyToOne
    private Account account;

}
