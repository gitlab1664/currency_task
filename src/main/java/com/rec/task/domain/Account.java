package com.rec.task.domain;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Account {

    @Id
    @Setter(AccessLevel.NONE)
    private Long id;

    @OneToOne
    @MapsId
    private AppUser user;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "account")
    private Set<SubAccount> subAccounts = new HashSet<>();

    public void addSubAccount(SubAccount subAccount) {
        subAccount.setAccount(this);
        subAccounts.add(subAccount);
    }
}
