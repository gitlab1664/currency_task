package com.rec.task.domain.dto;

import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegisterInfoDto extends RepresentationModel<RegisterInfoDto> {

    @Valid
    @NotNull(message = "user info are required")
    private RegisterUserDto user;

    @NotNull(message = "initial amount in PLN is required")
    @PositiveOrZero(message = "initial amount must be equal or greater then 0")
    private BigDecimal amount;
}
