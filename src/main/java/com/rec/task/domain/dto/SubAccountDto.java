package com.rec.task.domain.dto;

import com.rec.task.domain.AccType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SubAccountDto {

    private BigDecimal amount;

    private AccType type;
}
