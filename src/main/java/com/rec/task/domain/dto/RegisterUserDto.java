package com.rec.task.domain.dto;

import com.rec.task.validators.AgeConstraint;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.pl.PESEL;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegisterUserDto {

    @PESEL
    @AgeConstraint
    @NotNull(message = "user personal number cannot be null")
    @NotBlank(message = "user personal number cannot be blank")
    protected String personalNumber;

    @NotNull(message = "user first name cannot be null")
    @NotBlank(message = "user first name cannot be blank")
    private String firstName;

    @NotNull(message = "user last name cannot be null")
    @NotBlank(message = "user last name cannot be blank")
    private String lastName;
}
