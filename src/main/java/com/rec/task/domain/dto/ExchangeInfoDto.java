package com.rec.task.domain.dto;

import com.rec.task.validators.CurrencyTypeConstraint;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExchangeInfoDto {

    @NotNull(message = "user identification number is required, and must not be null")
    @NotBlank(message = "user identification number is required, and must not be blank")
    private String userNumber;

    @NotNull(message = "amount to transfer must be specified, and must not be null")
    @PositiveOrZero(message = "amount to transfer must not be negative value")
    private BigDecimal amount;

    @CurrencyTypeConstraint
    @NotNull(message = "source currency type is required, and must not be null")
    @NotBlank(message = "source currency type is required, and must not be blank")
    private String sourceCurrency;

    @CurrencyTypeConstraint
    @NotNull(message = "target currency type is required, and must not be null")
    @NotBlank(message = "target currency type is required, and must not be blank")
    private String targetCurrency;
}
