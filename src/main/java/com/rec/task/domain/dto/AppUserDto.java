package com.rec.task.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AppUserDto extends RepresentationModel<AppUserDto> {

    private String firstName;
    private String lastName;
    private String personalNumber;
    private AccountDto account;
}
