package com.rec.task.domain.errors;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserExistsException extends RuntimeException{

    public UserExistsException(String message) {
        super(message);
        log.error("User already exists");
    }
}
