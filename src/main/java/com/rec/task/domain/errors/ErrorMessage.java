package com.rec.task.domain.errors;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Builder
@Getter
@Setter
public class ErrorMessage {

    private Boolean result;

    private LocalDateTime timeStamp;

    private List<String> errors;

}
