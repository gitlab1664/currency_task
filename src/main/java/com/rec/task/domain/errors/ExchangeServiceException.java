package com.rec.task.domain.errors;

public class ExchangeServiceException extends RuntimeException {

    public ExchangeServiceException(String message) {
        super(message);
    }
}
