package com.rec.task.mappers;

import com.rec.task.domain.AppUser;
import com.rec.task.domain.dto.AppUserDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AppUserMapper {

    AppUserMapper INSTANCE = Mappers.getMapper(AppUserMapper.class);

    AppUserDto toDto(AppUser appUser);

}
