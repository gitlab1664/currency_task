package com.rec.task.mappers;

import com.rec.task.domain.RegisterInfo;
import com.rec.task.domain.dto.RegisterInfoDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface RegisterInfoMapper {

    RegisterInfoMapper INSTANCE = Mappers.getMapper(RegisterInfoMapper.class);

    RegisterInfo fromDto(RegisterInfoDto registerInfoDto);
}
